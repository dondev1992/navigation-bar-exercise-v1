export const stuffImages = [
    {
        url: 'https://images.unsplash.com/photo-1512621776951-a57141f2eefd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=2070&q=80',
        name: 'Healthy Happy Salad',
        description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Tempora dolor animi ad perferendis dicta, accusamus aspernatur reiciendis porro, aliquid magni excepturi minus mollitia enim quas!'
    },
    {
        url: 'https://images.unsplash.com/photo-1547592180-85f173990554?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80',
        name: 'Healthy Rice and Beans',
        description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Tempora dolor animi ad perferendis dicta, accusamus aspernatur reiciendis porro, aliquid magni excepturi minus mollitia enim quas!'
    },
    {
        url: 'https://images.unsplash.com/photo-1521986329282-0436c1f1e212?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=2076&q=80',
        name: 'Healthy Food on Rye',
        description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Tempora dolor animi ad perferendis dicta, accusamus aspernatur reiciendis porro, aliquid magni excepturi minus mollitia enim quas!'
    },
    {
        url: 'https://images.unsplash.com/photo-1504674900247-0877df9cc836?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80',
        name: 'Healthy Beef and Peanuts',
        description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Tempora dolor animi ad perferendis dicta, accusamus aspernatur reiciendis porro, aliquid magni excepturi minus mollitia enim quas!'
    },
    {
        url: 'https://images.unsplash.com/photo-1498837167922-ddd27525d352?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80',
        name: 'Healthy Salad Bar',
        description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Tempora dolor animi ad perferendis dicta, accusamus aspernatur reiciendis porro, aliquid magni excepturi minus mollitia enim quas!'
    },
    {
        url: 'https://images.unsplash.com/photo-1455619452474-d2be8b1e70cd?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80',
        name: 'Healthy Veggie Soup',
        description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Tempora dolor animi ad perferendis dicta, accusamus aspernatur reiciendis porro, aliquid magni excepturi minus mollitia enim quas!'
    },
    {
        url: 'https://images.unsplash.com/photo-1540914124281-342587941389?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1074&q=80',
        name: 'Assorted Fruits and Veggies',
        description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Tempora dolor animi ad perferendis dicta, accusamus aspernatur reiciendis porro, aliquid magni excepturi minus mollitia enim quas!'
    },
    {
        url: 'https://images.unsplash.com/photo-1519708227418-c8fd9a32b7a2?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1170&q=80',
        name: 'Healthy Grilled Salmon',
        description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Tempora dolor animi ad perferendis dicta, accusamus aspernatur reiciendis porro, aliquid magni excepturi minus mollitia enim quas!'
    },
    {
        url: 'https://images.unsplash.com/photo-1608835291093-394b0c943a75?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1172&q=80',
        name: 'Healthy Roast Beef and Mac',
        description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Tempora dolor animi ad perferendis dicta, accusamus aspernatur reiciendis porro, aliquid magni excepturi minus mollitia enim quas!'
    },
    {
        url: 'https://images.unsplash.com/photo-1627309302198-09a50ae1b209?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=1974&q=80',
        name: 'Healthy Veggie Salad and Sandwich',
        description: 'Lorem ipsum dolor sit amet consectetur adipisicing elit. Tempora dolor animi ad perferendis dicta, accusamus aspernatur reiciendis porro, aliquid magni excepturi minus mollitia enim quas!'
    },
]