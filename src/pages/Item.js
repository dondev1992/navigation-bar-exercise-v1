import React, { useEffect } from "react";
import { useLocation, useNavigate } from "react-router-dom";

const Item = () => {
  const location = useLocation();
  const navigate = useNavigate();

  const itemContainer = {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  };

  useEffect(() => {
    if (location.state === null) {
      // console.log(location.state)
      navigate("/*");
    }
  }, [location.state]);

  return (
    <div style={itemContainer}>
      <h1 style={{ margin: "3rem" }}>{location?.state?.name}</h1>
      <img
        src={`${location?.state?.url}`}
        alt={`${location?.state?.name}`}
        style={{ height: "100%", width: "800px" }}
      />
      <p style={{ width: "800px" }}>{location?.state?.description}</p>
    </div>
  );
};

export default Item;
