import { useState } from "react";
import { useNavigate } from "react-router-dom";

const Login = ({ addUsername }) => {
  const [name, setName] = useState("");
  const navigate = useNavigate();

  const formContainer = {
    display: "flex",
    flexDirection: "column",
    alignItems: "center",
  };

  const formStyle = {
    display: "flex",
    flexDirection: "column",
    alignItems: "flex-start",
  };

  const login = () => {
    addUsername(name);
    navigate("/food", { state: name });
  };

  return (
    <div style={formContainer}>
      <h1>Login</h1>
      <form style={formStyle} onSubmit={login}>
        <label style={{ marginBottom: ".15rem" }} htmlFor="username">
          Username
        </label>
        <input
          style={{ marginBottom: "1.5rem", padding: "5px" }}
          type="text"
          name="username"
          onChange={(e) => setName(e.target.value)}
          value={name}
        />
        <label style={{ marginBottom: ".15rem" }} htmlFor="password">
          Password
        </label>
        <input
          style={{ marginBottom: "1.5rem", padding: "5px" }}
          type="password"
          name="password"
        />
        <button style={{ padding: "5px 10px", cursor: "pointer" }}>
          Login
        </button>
      </form>
    </div>
  );
};

export default Login;
