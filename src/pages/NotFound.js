import React from 'react'

const NotFound = () => {
    const notFoundStyling = {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        justifyContent: 'center',
        margin: '5em'
    }

    return (
        <div>
            <h1 style={notFoundStyling}>404 Not Found</h1>
        </div>

    )
}

export default NotFound