import classes from './Food.module.css';

const Food = () => {
    return (
        <div className={classes.container}>
            <h1 style={{ margin: '3rem' }} >Food, Food, Food!</h1>
            <img style={{ maxWidth: '60%' }} src="https://images.unsplash.com/photo-1571805341302-f857308690e3?ixlib=rb-4.0.3&ixid=MnwxMjA3fDB8MHxwaG90by1wYWdlfHx8fGVufDB8fHx8&auto=format&fit=crop&w=764&q=80" alt="food" />
        </div>

    )
}

export default Food