import { useNavigate } from 'react-router-dom';
import { stuffImages } from '../data/FoodItems';
import classes from './Stuff.module.css';

const Stuff = () => {
    const navigate = useNavigate();

    const viewItem = (item) => {
        try {
            navigate(`/stuff/${item.name}`, { state: item });
        } catch (error) {
            navigate('/*');
        }
    }

    return (
        <div className={classes.container}>
            <h1 style={{ margin: '3rem' }}>Our Stuff</h1>
            <div className={classes.stuff_item_grid}>
                {stuffImages.map((item, index) => (
                    <div key={index} className={classes.stuff_item_container}>
                        <img
                            src={item.url}
                            alt={item.name}
                            height={300}
                            width={500}
                            onClick={() => viewItem(item)}
                        />
                        <h2 className={classes.stuff_item_name}>{item.name}</h2>
                    </div>))}
            </div>

        </div>

    )
}

export default Stuff