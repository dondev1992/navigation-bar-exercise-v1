import { Route, Routes } from "react-router-dom";
import React, { useEffect, useState } from "react";
import NavBar from "./components/NavBar";
import "./App.css";
import Login from "./pages/Login";
import Food from "./pages/Food";
import Stuff from "./pages/Stuff";
import NotFound from "./pages/NotFound";
import Item from "./pages/Item";

function App() {
  const [username, setUsername] = useState("");
  const [isLogged, setIsLogged] = useState(false);

  const addUsername = (name) => {
    setUsername(name);
  };

  useEffect(
    (name) => {
      setUsername(name);
    },
    [isLogged]
  );

  return (
    <div className="App">
      <NavBar username={username} />
      <Routes>
        <Route path="/" element={<Login addUsername={addUsername} />} />
        <Route path="/food" element={<Food />} />
        <Route path="/stuff">
          <Route index element={<Stuff />} />
          <Route path=":item" element={<Item />} />
        </Route>
        <Route path="*" element={<NotFound />} />
      </Routes>
    </div>
  );
}

export default App;
