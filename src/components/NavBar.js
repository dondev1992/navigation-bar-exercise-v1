import { useEffect, useState } from "react";
import { NavLink } from "react-router-dom";
import classes from "./NavBar.module.css";

const NavBar = ({ username }) => {
  const [user, setUser] = useState("");
  const [isLoggedIn, isLoggedInSet] = useState(false);

  const navLinks = {
    display: "inline-block",
    margin: "1rem",
  };

  const activeLink = {
    fontWeight: "bold",
    borderBottom: "2px solid black",
  };

  const navStyle = {
    display: "flex",
    justifyContent: "space-between",
    alignItems: "center",
    backgroundColor: "#ADC178",
    padding: "0 2rem",
  };

  const logout = () => {
    setUser("");
    isLoggedInSet(false);
  };

  const login = () => {
    // loginFromNavBar();
    isLoggedInSet(true);
  };

  useEffect(() => {
    if (username === undefined || username === "") {
      return;
    } else {
      isLoggedInSet(true);
      setUser(username);
      // console.log(username);
    }
  }, [username]);

  return (
    <nav style={navStyle}>
      <h1 style={{ margin: "1rem", fontSize: "2rem" }}>Food and Stuff Inc.</h1>

      <ul>
        {isLoggedIn ? (
          <>
            {user && (
              <p
                style={{ display: "inline", marginRight: "3rem" }}
              >{`Hi ${user},`}</p>
            )}
            <li style={navLinks}>
              <NavLink
                className={classes.linkStyle}
                style={({ isActive }) => {
                  return isActive ? activeLink : {};
                }}
                to="/food"
              >
                Food
              </NavLink>
            </li>
            <li style={navLinks}>
              <NavLink
                className={classes.linkStyle}
                style={({ isActive }) => {
                  return isActive ? activeLink : {};
                }}
                to="/stuff"
              >
                Stuff
              </NavLink>
            </li>
            <li style={navLinks}>
              <NavLink
                className={classes.linkStyle}
                style={({ isActive }) => {
                  return isActive ? activeLink : {};
                }}
                to="/"
                onClick={logout}
              >
                Logout
              </NavLink>
            </li>
          </>
        ) : (
          <li style={navLinks}>
            <NavLink
              className={classes.linkStyle}
              style={({ isActive }) => {
                return isActive ? activeLink : {};
              }}
              to="/food"
              onClick={login}
            >
              Login
            </NavLink>
          </li>
        )}
      </ul>
    </nav>
  );
};

export default NavBar;
